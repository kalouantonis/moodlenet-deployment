# MoodleNet Deployment

Ansible playbooks for deploying MoodleNet.

## Quickstart

Below, we run the ansible playbook on a local vagrant machine. This can be set up by using
`vagrant up` in the local directory, check the `Vagrantfile` for more details.

*Note:* Currently only the backend is working.

```bash
$ ansible-playbook -i inventory/vagrant playbook/moodlenet.yml
```

## Contributing

Only one note here, **never ever commit secrets to git!!1!**, use Ansible vault (or similar) instead.
